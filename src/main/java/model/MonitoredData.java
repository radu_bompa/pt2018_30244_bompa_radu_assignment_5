package model;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * Created by Radu Bompa
 * 5/10/2018
 */
public class MonitoredData {
	private LocalDateTime startTime;
	private LocalDateTime endTime;
	private String activity;

	public LocalDateTime getStartTime() {
		return startTime;
	}

	public void setStartTime(LocalDateTime startTime) {
		this.startTime = startTime;
	}

	public LocalDateTime getEndTime() {
		return endTime;
	}

	public void setEndTime(LocalDateTime endTime) {
		this.endTime = endTime;
	}

	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}
}
