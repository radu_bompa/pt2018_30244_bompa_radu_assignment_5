package business;

import model.MonitoredData;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Radu Bompa
 * 5/10/2018
 */
public class MainController {

	private List<MonitoredData> monitoredDataList = new LinkedList<>();

	public static void main(String[] args) {
		MainController mainController = new MainController();
		mainController.setMonitoredDataList(mainController.citireFisier());

		mainController.exercitiul1();
		mainController.exercitiul2();
		mainController.exercitiul3();
		mainController.exercitiul4();
		mainController.exercitiul5();
	}

	/**
	 * Read the data from the file Activity.txt using streams and create a list of objects of type MonitoredData
	 *
	 * @return lista de MonitoredData
	 */
	private List<MonitoredData> citireFisier() {
		try {
			return Files.lines(Paths.get("Activities.txt")).map(line -> {
				String[] info = line.split("\t\t");
				MonitoredData monitoredData = new MonitoredData();
				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
				try {
					Date in = dateFormat.parse(info[0]);
					LocalDateTime ldt = LocalDateTime.ofInstant(in.toInstant(), ZoneId.systemDefault());
					monitoredData.setStartTime(ldt);
					in = dateFormat.parse(info[1]);
					ldt = LocalDateTime.ofInstant(in.toInstant(), ZoneId.systemDefault());
					monitoredData.setEndTime(ldt);
				} catch (ParseException e) {
					e.printStackTrace();
				}
				monitoredData.setActivity(info[2]);
				return monitoredData;
			}).collect(Collectors.toList());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 1. Count the distinct days that appear in the monitoring data
	 */
	private void exercitiul1() {
		long count = getMonitoredDataList()
				.stream()
				.collect(LinkedHashSet::new, (l, elem) -> {
					l.add(elem.getStartTime().getYear() + " " + elem.getStartTime().getDayOfYear());
					l.add(elem.getEndTime().getYear() + " " + elem.getEndTime().getDayOfYear());
				}, LinkedHashSet::addAll)
				.size();
		System.out.println("Distinct days: " + count);
	}

	/**
	 * 2. Determine a map of type <String, Integer> that maps to each distinct action type the number of occurrences in the log.
	 * Write the resulting map into a text file
	 */
	private void exercitiul2() {
		Map<String, Integer> activitiesCount = getMonitoredDataList()
				.stream()
				.collect(Collectors.groupingBy(MonitoredData::getActivity,
						Collectors.mapping(MonitoredData::getActivity, Collectors.toList())))
				.entrySet()
				.stream()
				.collect(LinkedHashMap::new, (k, v) -> {
					k.put(v.getKey(), v.getValue().size());
				}, LinkedHashMap::putAll);
		writeToFile("exercitiul2.txt", activitiesCount.toString());
	}

	/**
	 * 3. Generates a data structure of type Map<Integer, Map<String, Integer>> that
	 * contains the activity count for each day of the log (task number 2 applied for each
	 * day of the log) and writes the result in a text file
	 */
	private void exercitiul3() {
		Map<Integer, Map<String, Integer>> activitiesDailyCount = getMonitoredDataList()
				.stream()
				.collect(Collectors.groupingBy(m -> m.getStartTime().getDayOfMonth(),
						Collectors.groupingBy(MonitoredData::getActivity,
								Collectors.mapping(MonitoredData::getActivity, Collectors.toList()))))
				.entrySet()
				.stream()
				.collect(LinkedHashMap::new, (k, v) -> {
					k.put(v.getKey(),
							v.getValue()
									.entrySet()
									.stream()
//									avem Map<String, List<String>> si colectam ca sa avem size-ul listei ca value
									.collect(LinkedHashMap::new, (k1, v1) -> {
										k1.put(v1.getKey(), v1.getValue().size());
									}, LinkedHashMap::putAll));
				}, LinkedHashMap::putAll);
		writeToFile("exercitiul3.txt", activitiesDailyCount.toString());
	}

	/**
	 * 4. Determine a data structure of the form Map<String, DateTime> that maps
	 * for each activity the total duration computed over the monitoring period. Filter the
	 * activities with total duration larger than 10 hours. Write the result in a text file.
	 */
	private void exercitiul4() {
		Map<String, Duration> activitiesDuration = getMonitoredDataList()
				.stream()
				.collect(Collectors.groupingBy(MonitoredData::getActivity,
						Collectors.mapping(m -> Duration.between(m.getStartTime(), m.getEndTime()).abs(), Collectors.toList())))
				.entrySet()
				.stream()
				.collect(LinkedHashMap::new,
						(k, v) -> k.put(
								v.getKey(),
								Duration.of((Long) v.getValue()
										.stream()
										.filter(duration -> duration.compareTo(Duration.of(36000, ChronoUnit.SECONDS)) <= 0)
										.mapToLong(k1 -> k1.get(ChronoUnit.SECONDS))
										.sum(), ChronoUnit.SECONDS)), LinkedHashMap::putAll);
		String actDurationString = "";
		for (Map.Entry<String, Duration> activityDuration : activitiesDuration.entrySet()) {
			actDurationString += activityDuration.getKey() + " -> " + (activityDuration.getValue().toHours() != 0 ? activityDuration.getValue().toHours() + " hours " : "")
					+ (activityDuration.getValue().toHours() != 0 && activityDuration.getValue().toMinutes() != 0 ? activityDuration.getValue().toMinutes() % 60 + " minutes " : "")
					+ activityDuration.getValue().getSeconds() % 60 + " seconds\n";
		}
		writeToFile("exercitiul4.txt", actDurationString);
	}

	/**
	 * 5. Filter the activities that have 90% of the monitoring samples with duration
	 * less than 5 minutes, collect the results in a List<String> containing only the
	 * distinct activity names and write the result in a text file.
	 */
	private void exercitiul5() {
		List<String> shortActivities = getMonitoredDataList()
				.stream()
				.collect(Collectors.groupingBy(MonitoredData::getActivity,
						Collectors.mapping(m -> Duration.between(m.getStartTime(), m.getEndTime()).abs(), Collectors.toList())))
				.entrySet()
				.stream()
				.filter(m -> {
					int shortActs = m.getValue().stream().filter(act -> act.getSeconds() <= 300).collect(Collectors.toList()).size();
					return ((double) shortActs) / ((double)m.getValue().size()) > 0.9;
				}).map(Map.Entry::getKey).collect(Collectors.toList());
		writeToFile("exercitiul5.txt", shortActivities.toString());
	}

	private void writeToFile(String filename, String content) {
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(filename));
			writer.write(content);
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public List<MonitoredData> getMonitoredDataList() {
		return monitoredDataList;
	}

	public void setMonitoredDataList(List<MonitoredData> monitoredDataList) {
		this.monitoredDataList = monitoredDataList;
	}
}
